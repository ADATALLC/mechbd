﻿using System.Drawing;
using System.Windows.Forms;

namespace MechBD
{
    public partial class GameView : Form
    {
        public GameView()
        {
            InitializeComponent();            
        }

        public void DrawFrame(GameScene scene)
        {
            Rectangle src = new Rectangle(Point.Empty, Properties.Settings.Default.Resolution);
            Rectangle dst = new Rectangle(Point.Empty, ClientSize);

            using (Bitmap b = new Bitmap(src.Width, src.Height))
            {
                using (Graphics frame = Graphics.FromImage(b))
                {
                    frame.Clear(Color.Black);
                    scene?.Draw(frame);
                }

                CreateGraphics().DrawImage(b, dst, src, GraphicsUnit.Pixel);                
            }
        }
    }
}
