﻿using System.Drawing;
using System.Drawing.Text;

namespace MechBD
{
    internal class GameFont
    {
        public static GameFont Instance = new GameFont();
        public readonly FontFamily FontFamily;

        private GameFont()
        {
            PrivateFontCollection fonts = new PrivateFontCollection();
            fonts.AddFontFile("./Assets/Fonts/Romulus.ttf");

            FontFamily = fonts.Families.Length == 0 ? FontFamily.GenericMonospace : fonts.Families[0];
        }
    }
}
