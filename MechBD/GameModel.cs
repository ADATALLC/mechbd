﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using MechBD.Components;

namespace MechBD
{
    public partial class GameModel
    {
        public GameModel()
        {
            Scenes = new List<GameScene>();
        }
    }

    public partial class GameModel : IMouseInputComponent
    {
        public void OnMouseClicked(Point position, MouseButtons button)
        {
            ActiveScene?.Children.FindAll(c => c.IsActive && c is IMouseInputComponent).ForEach(c => ((IMouseInputComponent)c)?.OnMouseClicked(position, button));
        }

        public void OnMouseMoved(Point position)
        {
            ActiveScene?.Children.FindAll(c => c.IsActive && c is IMouseInputComponent).ForEach(c => ((IMouseInputComponent)c)?.OnMouseMoved(position));
        }
    }

    public partial class GameModel : IKeyboardInputComponent
    { 
        public void OnKeyPressed(Keys button)
        {
            foreach (GameObject child in ActiveScene?.Children) if (child.IsActive && child is IKeyboardInputComponent) ((IKeyboardInputComponent)child)?.OnKeyPressed(button);
        }
    }

    public partial class GameModel
    {
        public List<GameScene> Scenes { get; private set; }
        public GameScene ActiveScene { get; private set; }

        public GameModel AddScene(GameScene scene)
        {
            if (!Scenes.Contains(scene)) Scenes.Add(scene);

            return this;
        }

        public GameModel AddScenes(GameScene[] scenes)
        {
            Array.ForEach(scenes, s => AddScene(s));

            return this;
        }

        public GameModel RemoveScene(GameScene scene)
        {
            Scenes.Remove(scene);         

            return this;
        }

        public void ActivateScene(GameSceneID id)
        {
            ActivateScene(Scenes.Find(s => s.Id == id));
        }

        public void ActivateScene(GameScene scene = null)
        {
            ActiveScene?.OnDeactivation();
            ActiveScene = scene ?? ActiveScene;
            ActiveScene?.OnActivation();
        }
    }
}
