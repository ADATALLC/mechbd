﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MechBD
{
    public class GameCamera
    {
        public static GameCamera Main = new GameCamera();

        private Form _view;

        private Rectangle _margins;
        
        public double Speed { get; set; }
        public Point Position { get; set; }
        public PointF Scale { get; private set; }
        public Size Size { get; private set; }
        public GameObject Target { get; set; }
        public bool Fixed { get; set; }

        private GameCamera()
        {
            Speed = 2;
            Position = Point.Empty;
            Scale = new PointF(1, 1);
            Size = Properties.Settings.Default.Resolution;

            Target = null;
            Fixed = true;

            _margins = new Rectangle(100, 100, 100, 100);
        }

        public void AttachTo(Form view)
        {
            DetachFrom(_view);

            _view = view;
            _view.SizeChanged += OnSizeChanged;
        }

        public void DetachFrom(Form view)
        {
            if (view != null) view.SizeChanged -= OnSizeChanged;
        }

        private void OnSizeChanged(object sender, EventArgs e)
        {
            Scale = new PointF(
                _view.ClientSize.Width / (float)Size.Width,
                _view.ClientSize.Height / (float)Size.Height
            );
        }

        public void Update()
        {
            if (Fixed) return;

            if (Target == null)
            {
                CheckKeyboardInput();
                CheckMouseInput();
            } else {
                FollowTarget();
            }
        }

        private void CheckKeyboardInput()
        {
            List<Keys> buttons = GameInput.Keyboard.Buttons;
            if (buttons.Contains(Keys.Up)) MoveCameraUp();
            if (buttons.Contains(Keys.Down)) MoveCameraDown();
            if (buttons.Contains(Keys.Left)) MoveCameraLeft();
            if (buttons.Contains(Keys.Right)) MoveCameraRight();
        }

        private void CheckMouseInput()
        {
            Point position = GameInput.Mouse.Position;
            if (position.X < _margins.X) MoveCameraLeft();
            if (position.X > _view.ClientSize.Width - _margins.Width) MoveCameraRight();
            if (position.Y < _margins.Y) MoveCameraUp();
            if (position.Y > _view.ClientSize.Height - _margins.Height) MoveCameraDown();
        }

        private void FollowTarget()
        {
            var x = Size.Width / 2 - Target.Position.X;
            var y = Size.Height / 2 - Target.Position.Y;
            Position = new Point(x, y);
        }

        private void MoveCameraUp()
        {
            Position = Position.Offset2(0, (int)(1 * GameTimer.Instance.DeltaTime * Speed));
        }

        private void MoveCameraDown()
        {
            Position = Position.Offset2(0, (int)(-1 * GameTimer.Instance.DeltaTime * Speed));
        }

        private void MoveCameraRight()
        {
            Position = Position.Offset2((int)(-1 * GameTimer.Instance.DeltaTime * Speed), 0);
        }

        private void MoveCameraLeft()
        {
            Position = Position.Offset2((int)(1 * GameTimer.Instance.DeltaTime * Speed), 0);
        }

        public bool IsObjectShown(Rectangle drawablebounds)
        {
            return new Rectangle(Position, _view.ClientSize).IntersectsWith(SafeMarginsFor(drawablebounds));
        }

        private Rectangle SafeMarginsFor(Rectangle bounds)
        {
            Rectangle margins = new Rectangle(Position, Size);
            margins.Inflate(bounds.Width / 2, bounds.Height / 2);
            return margins;
        }
    }
}
