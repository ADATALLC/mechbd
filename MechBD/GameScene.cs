﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MechBD
{
    public partial class GameScene
    {
        public GameSceneID Id { get; private set; }

        public GameScene(GameSceneID id)
        {
            Id = id;
            Children = new List<GameObject>();
        }

        public static bool operator ==(GameScene s1, GameScene s2) => s1?.Id == s2?.Id;
        public static bool operator !=(GameScene s1, GameScene s2) => !(s1 == s2);

        public override bool Equals(object obj) => obj != null && obj is GameScene && (GameScene)obj == this;
        public override int GetHashCode() => base.GetHashCode();
    }

    public partial class GameScene
    {
        public List<GameObject> Children { get; private set; }

        public void AddChild(GameObject child)
        {
            if(!Children.Contains(child)) Children.Add(child);            
        }

        public void AddChildren(GameObject[] children)
        {
            Array.ForEach(children, c => AddChild(c));
        }

        public void RemoveChild(GameObject child)
        {
            Children.Remove(child);         
        }
    }

    public partial class GameScene
    {
        public void Update(double deltatime)
        {
            Children.FindAll(c => c.IsActive).ForEach(c => c.Update(deltatime));
        }

        public void Draw(Graphics gc)
        {
            Children.FindAll(c => c.IsActive).ForEach(c => c.Draw(gc));
            PrintStats(gc);
        }

        private static void PrintStats(Graphics gc)
        {
            string str = $"FPS: {GameTimer.Instance.FPS} Scale: {GameCamera.Main.Scale} Mouse: {GameInput.Mouse?.Position } Camera: {GameCamera.Main.Position}";
            gc.DrawString(str, new Font(GameFont.Instance.FontFamily, 12f), new SolidBrush(Color.Red), PointF.Empty);
        }
    }

    public partial class GameScene
    {
        public virtual void Init()
        {
            throw new NotImplementedException("GameScene: Init() method not implemented");
        }

        public virtual void OnActivation()
        {
        }

        public virtual void OnDeactivation()
        {
        }
    }
}
