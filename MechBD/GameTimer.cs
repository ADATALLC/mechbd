﻿using System;
using System.Timers;

namespace MechBD
{
    public class GameTimer : Timer
    {
        public static GameTimer Instance = new GameTimer();

        public delegate void OnUpdateEventHandler(double deltatime);
        public OnUpdateEventHandler OnUpdateEvent;

        public double DeltaTime;

        public int FPS => (int)(_frpCurrent);
        private double _frpCurrent;
        private double _fpsTotal;

        private double _frameThr;
        private DateTime _lastEvent;
        private DateTime _lastFPSRefresh;

        private GameTimer() : base(1000 / Properties.Settings.Default.GameFrameRate)
        {
            GameTimerService.Instance.RegisterTimer(this);

            _frameThr = 2 * Interval;
            _lastEvent = DateTime.Now;
            _lastFPSRefresh = DateTime.Now;

            _frpCurrent = 0;
            _fpsTotal = 0;

            Elapsed += OnTimerTick;
        }        

        private void OnTimerTick(object sender, ElapsedEventArgs e)
        {
            if (_fpsTotal > 0 && !GameInput.Mouse.IsMouseInGameBounds) return;

            RaiseUpdateEvent(e.SignalTime);
            CountFPS(e.SignalTime);
        }

        private void RaiseUpdateEvent(DateTime time)
        {
            double delta = time.Subtract(_lastEvent).TotalMilliseconds;

            _lastEvent = time;

            if (delta < _frameThr)
            {
                DeltaTime = delta;
                OnUpdateEvent?.Invoke(delta / 1000);
            }
        }

        private void CountFPS(DateTime time)
        {
            _fpsTotal++;

            if ((time - _lastFPSRefresh).TotalMilliseconds > 1000)
            {
                _frpCurrent = _fpsTotal;
                _fpsTotal = 0;
                _lastFPSRefresh = time;
            }
        }
    }
}
