﻿using System;
using System.Windows.Forms;

using MechBD.Scenes;

namespace MechBD
{
    static class Game
    {
        public static GameController Controller;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            GameScene[] scenes = {
                new MainMenu_Scene(),
                new OptionsMenu_Scene(),
                new LevelMap_Scene()
            };

            GameView view = new GameView();
            Controller = new GameController(view, scenes[0], scenes);

            Application.Run(view);
        }

        public static void Exit()
        {
            GameTimer.Instance.Stop();
            GameSoundManager.Current.Stop();
            GameTimerService.Instance.StopAll();

            Application.Exit();
        }
    }
}
