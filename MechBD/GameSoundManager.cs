﻿using System.IO;
using System.Media;
using NAudio.Wave;

using MechBD.Components;
using System;

namespace MechBD
{
    class GameSoundManager
    {
        public static GameSoundManager Current = new GameSoundManager();

        private readonly SoundPlayer _sound;
        private readonly IWavePlayer _music;
        private WaveFileReader _musicStream;

        public ObservableFloatProperty MusicVolume { get; set; }

        private GameSoundManager()
        {
            _sound = new SoundPlayer();
            _music = new WaveOut();
            _music.PlaybackStopped += ReplayMusic;

            MusicVolume = new ObservableFloatProperty(0f);
            MusicVolume.OnObservalblePropertyChanged += OnMusicLevelChanged;
            MusicVolume.Value = 0.3f;            
        }

        private void OnMusicLevelChanged(object property)
        {
            if (_music != null && property == MusicVolume) _music.Volume = MusicVolume.Value;
        }

        public void PlayMusic(Stream music)
        {
            _music.Stop();
            _musicStream?.Dispose();
            _musicStream = new WaveFileReader(music);
            _music.Init(_musicStream);
            _music.Play();
        }

        private void ReplayMusic(object sender, StoppedEventArgs e)
        {
            try
            {
                if (_musicStream != null) _musicStream.Position = 0;
                _music?.Play();
            } catch {}
        }

        public void PlaySound(Stream sound)
        {
            _sound?.Stop();
            _sound.Stream = sound;
            _sound.Play();
        }

        public void Stop()
        {
            _sound?.Stop();
            _sound?.Dispose();
            _music?.Stop();
            _musicStream?.Dispose();
            _music?.Dispose();
        }
    }
}
