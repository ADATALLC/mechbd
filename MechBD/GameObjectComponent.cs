﻿namespace MechBD
{
    public class GameObjectComponent
    {
        protected GameObject _parent;

        public bool IsActive;

        public GameObjectComponent(GameObject parent)
        {
            _parent = parent;
            _parent.AddComponent(this);
            IsActive = true;
        }
    }
}
