﻿using System;
using System.Drawing;
using System.Windows.Forms;

using MechBD.Components;
using MechBD.Objects;

namespace MechBD.ObjectsUI
{
    public partial class UIBar : GameObject
    {
        private UIImage _background;
        private UIImage _filler;
        private Size _fillerSize = new Size(132, 14);

        private ObservableFloatProperty _property;
        private float _max, _min;

        public float Max
        {
            get => _max;
            set
            {
                _max = value;
                if (_property.Value > _max) _property.Value = _max;
            }
        }

        public float Min
        {
            get => _min;
            set
            {
                _min = value;
                if (_property.Value < _min) _property.Value = _min;
            }
        }


        public float Value {
            get => _property.Value;
            set
            {
                float newV = (value > _max) ? _max : ((value < _min) ? _min : value);
                _property.Value = value;
            }
        }

        public UIBar(Point position, Size size, float min, float max, ObservableFloatProperty property) : base()
        {
            _max = max;
            _min = min;
            _property = property;

            RenderType = GameRenderType.Static;

            Color = Color.Red;
            Position = position;
            Size = size;

            Rectangle a = new Rectangle(Position, Size);
            Rectangle b = a; 
            b.Inflate(-9, -3);

            _background = new UIImage(Properties.Resources.ui, new Rectangle(new Point(450, 140), Size), a);
            _filler = new UIImage(Properties.Resources.ui, new Rectangle(new Point(459, 164), _fillerSize), b);

            AddChildren(new GameObject[] { _background, _filler });
        }

        public override void Update(double deltatime)
        {
            base.Update(deltatime);

            float value = _property.Value / _max;

            _filler.Size.Width = (int)(_fillerSize.Width * value);
        }
    }

    public partial class UIBar : IMouseInputComponent
    {
        public void OnMouseClicked(Point position, MouseButtons button)
        {
            UpdateMyProperty(position);
        }

        public void OnMouseMoved(Point position)
        {
            if (GameInput.Mouse.IsLeftMouseDown) UpdateMyProperty(position);
        }

        private void UpdateMyProperty(Point position)
        {
            if (!IsMouseCursorInMyBounds()) return;
            
            float value = ((float)position.X - _filler.Position.X) / _fillerSize.Width;
            _property.Value = (value > Max) ? Max : (value < Min ? Min : value);            
        }
    }
}
