﻿using System.Drawing;

using MechBD.Objects;

namespace MechBD.ObjectsUI
{
    class UIImage : GameObject
    {
        private Sprite _sprite;

        public Bitmap Image
        {
            get => _sprite.Image;
            set => _sprite.Image = value;
        }

        public UIImage(Bitmap image, Point position, Size size) : this(image, position)
        {
            Size = size;
        }

        public UIImage(Bitmap image, Point position) : this(image)
        {
            Position = position;
        }

        public UIImage(Bitmap image) : base()
        {
            RenderType = GameRenderType.Static;

            Color = Color.Black;
            Position = Point.Empty;
            Size = image.Size;

            _sprite = new Sprite(this, image);

            AddComponent(_sprite);
        }

        public UIImage(Bitmap image, Rectangle source, Rectangle placement) : this(image, source, placement.Location, placement.Size)
        {
        }

        public UIImage(Bitmap image, Rectangle source, Point position, Size size) : this(image, source, position)
        {
            Size = size;
        }

        public UIImage(Bitmap image, Rectangle source, Point position) : this(image, source)
        {
            Position = position;
        }

        public UIImage(Bitmap image, Rectangle source) : base()
        {
            RenderType = GameRenderType.Static;

            Color = Color.Black;
            Position = Point.Empty;
            Size = source.Size;

            _sprite = new Sprite(this, image, source);

            AddComponent(_sprite);
        }
    }
}
