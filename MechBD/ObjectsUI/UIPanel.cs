﻿using System;
using System.Drawing;

using MechBD.Objects;

namespace MechBD.ObjectsUI 
{
    class UIPanel : GameObject
    {
        public UILabel Title { get; private set; }
        
        private Sprite _sprite;

        public UIPanel(string title, Point position, Size size) : this(title, position)
        {
            Size = size;
        }

        public UIPanel(string title, Point position) : this(title)
        {
            Position = position;
        }

        public UIPanel(string title) : base()
        {
            RenderType = GameRenderType.Static;

            Color = Color.Black;
            Position = new Point(100, 100);
            Size = new Size(200, 18);

            AddPanelImage();
            AddPanelTitle(title);
        }

        private void AddPanelTitle(string title)
        {
            Title = new UILabel(title);
            Title.Text.TextFontHeight = 18f;
            AddChild(Title);
            UpdateTitlePosition();
        }

        private void AddPanelImage()
        {
            _sprite = new Sprite(this, Properties.Resources.ui, new Rectangle(170, 270, 260, 300));
            AddComponent(_sprite);
        }

        public override void Update(double deltatime)
        {
            base.Update(deltatime);
            UpdateTitlePosition();
        }

        private void UpdateTitlePosition()
        {
            Size size = new Size((int)(Size.Width * 0.8), 26);
            Point position = new Point(Position.X + (Size.Width - size.Width) / 2, Position.Y - 10);

            Title.Size = size;
            Title.Position = position;
        }
    }
}
