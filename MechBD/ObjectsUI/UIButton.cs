﻿using System.Drawing;
using System.Windows.Forms;

using MechBD.Components;
using MechBD.Events;
using MechBD.Objects;

namespace MechBD.ObjectsUI
{
    public partial class UIButton : GameObject
    {
        public ScreenText Text => _text;

        private ScreenText _text;
        private Sprite _normal, _pressed;

        public Color NormalColor, PressedColor;

        public OnMouseClickedEventHandler OnClick;

        public bool ShowBackground { get; set; }

        public UIButton(string text, Point position, Size size) : this(text, position)
        {
            Size = size;
        }

        public UIButton(string text, Point position) : this(text)
        {
            Position = position;
        }

        public UIButton(string text) : base()
        {
            ShowBackground = true;

            RenderType = GameRenderType.Static;

            Color = Color.Goldenrod;
            Position = new Point(100, 100);
            Size = new Size(200, 40);

            _normal = new Sprite(this, Properties.Resources.ui, new Rectangle(450, 45, 200, 40));
            _pressed = new Sprite(this, Properties.Resources.ui, new Rectangle(450, 90, 200, 40)) { IsActive = false };

            NormalColor = Color.Black;
            PressedColor = Color.Goldenrod;

            _text = new ScreenText(this, text);

            AddComponents(new GameObjectComponent[] { _text, _normal, _pressed });
        }

        public override void Update(double deltatime)
        {
            base.Update(deltatime);

            if (!ShowBackground) return;

            _normal.IsActive = !IsMouseCursorInMyBounds();
            _pressed.IsActive = !_normal.IsActive;

            Color = _normal.IsActive ? NormalColor : PressedColor;
        }
    }

    public partial class UIButton : IMouseInputComponent
    {
        public void OnMouseClicked(Point position, MouseButtons button)
        {
            if (IsMouseCursorInMyBounds())
            {
                OnClick?.Invoke(position, button);
                GameSoundManager.Current.PlaySound(Properties.Resources.click);
            }
        }

        public void OnMouseMoved(Point position)
        {
            return;
        }
    }
}
