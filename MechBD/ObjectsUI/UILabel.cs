﻿using System.Drawing;

using MechBD.Objects;

namespace MechBD.ObjectsUI
{
    class UILabel : GameObject
    {
        public ScreenText Text => _text;

        public bool ShowBackgroudPanel
        {
            get => (_sprite == null) ? false : _sprite.IsActive;
            set
            {
                if (_sprite != null) _sprite.IsActive = value;
            }
        }

        private ScreenText _text;
        private Sprite _sprite;

        public UILabel(string text, Point position, Size size) : this(text, position)
        {
            Size = size;
        }

        public UILabel(string text, Point position) : this(text)
        {
            Position = position;
        }

        public UILabel(string text) : base()
        {
            ShowBackgroudPanel = true;

            RenderType = GameRenderType.Static;

            Color = Color.Goldenrod;
            Position = new Point(100, 100);
            Size = new Size(200, 18);

            _sprite = new Sprite(this, Properties.Resources.ui, new Rectangle(450, 10, 200, 18));
            _text = new ScreenText(this, text);

            AddComponents(new GameObjectComponent[] { _text, _sprite });
        }
    }
}
