﻿using System.Drawing;
using System.Windows.Forms;

namespace MechBD.Events
{
    public delegate void OnKeyboardKeyPressedEventHandler(Keys button);
    public delegate void OnMouseMovedEventHandler(Point position);
    public delegate void OnMouseClickedEventHandler(Point position, MouseButtons button);
}
