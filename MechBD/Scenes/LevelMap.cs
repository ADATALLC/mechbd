﻿using System.Drawing;
using MechBD.Objects;

namespace MechBD.Scenes
{
    public partial class LevelMap_Scene : GameScene
    {
        private TiledMap mapLevel;
        private GameCharacter player;

        public LevelMap_Scene() : base(GameSceneID.LevelMap)
        {
            Init();
        }

        public override void OnActivation()
        {
            GameCamera.Main.Target = player;
            GameCamera.Main.Fixed = false;
        }

        public override void OnDeactivation()
        {
            GameCamera.Main.Target = null;
            GameCamera.Main.Position = Point.Empty;
            GameCamera.Main.Fixed = true;
        }
    }
}
