﻿using MechBD.ObjectsUI;

namespace MechBD.Scenes
{
    public partial class OptionsMenu_Scene : GameScene
    {
        private UIImage imgBackground;
        private UIPanel pnlMain;
        private UILabel lblMusicVolume;
        private UIImage imgMusicOff, imgMusicOn;
        private UIBar barMusicLevel;
        private UIButton btnBack;

        public OptionsMenu_Scene() : base(GameSceneID.OptionsMenu)
        {
            Init();
        }
    }
}
