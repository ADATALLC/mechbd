﻿using MechBD.ObjectsUI;

namespace MechBD.Scenes
{
    public partial class MainMenu_Scene : GameScene
    {
        private UIImage imgBackground;
        private UIPanel pnlMainMenu;
        private GameObject imgMech;
        private UIButton btnStartGame, btnOptions, btnExit;

        public MainMenu_Scene() : base(GameSceneID.MainMenu)
        {
            Init();
        }        
    }
}
