﻿namespace MechBD
{
    public enum GameSceneID : byte
    {
        None, MainMenu, OptionsMenu, LevelMap
    }
}
