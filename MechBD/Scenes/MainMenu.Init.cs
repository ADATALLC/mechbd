﻿using System.Drawing;
using System.Drawing.Imaging;

using MechBD.Objects;
using MechBD.ObjectsUI;

namespace MechBD.Scenes
{
    public partial class MainMenu_Scene
    {
        public override void Init()
        {
            imgBackground = new UIImage(Properties.Resources.background);

            pnlMainMenu = new UIPanel("Mech BD", new Point(270, 100), new Size(260, 360));

            imgMech = new GameObject()
            {
                Size = new Size(200, 200),
                Position = new Point(300, 110),
                RenderType = GameRenderType.Static
            };
            Bitmap a = new Bitmap(Properties.Resources.Mech_Walk).Clone(new Rectangle(0, 0, 48, 48), PixelFormat.Format32bppArgb);
            Bitmap c = new Bitmap(Properties.Resources.Mech_Walk).Clone(new Rectangle(96, 0, 48, 48), PixelFormat.Format32bppArgb);
            AnimatedSprite anim = new AnimatedSprite(imgMech, new Bitmap[] { a, c });

            btnStartGame = new UIButton("Start new game", new Point(300, 280))
            {
                OnClick = delegate
                {
                    Game.Controller.Model.ActivateScene(GameSceneID.LevelMap);
                }
            };
            btnOptions = new UIButton("Options", new Point(300, 340))
            {
                OnClick = delegate 
                {
                    Game.Controller.Model.ActivateScene(GameSceneID.OptionsMenu);
                }
            };
            btnExit = new UIButton("Exit", new Point(300, 400))
            {
                OnClick = delegate 
                {
                    Game.Exit();
                }
            };

            AddChildren(new GameObject[] { imgBackground, pnlMainMenu, imgMech, btnStartGame, btnOptions, btnExit });

            GameSoundManager.Current.PlayMusic(Properties.Resources.bgmTitle);
        }
    }
}
