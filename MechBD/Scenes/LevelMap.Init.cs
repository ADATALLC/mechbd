﻿
using System.Drawing;
using MechBD.Objects;

namespace MechBD.Scenes
{
    public partial class LevelMap_Scene
    {
        public override void Init()
        {
            mapLevel = new TiledMap(Properties.Resources.level01);
            player = new GameCharacter(3, 0.3)
            {
                Position = new Point(376, 280)
            };

            AddChildren(new GameObject[] { mapLevel, player });
        }
    }
}
