﻿using System.Drawing;

using MechBD.Objects;
using MechBD.ObjectsUI;

namespace MechBD.Scenes
{
    public partial class OptionsMenu_Scene
    {
        public override void Init()
        {
            imgBackground = new UIImage(Properties.Resources.background);

            pnlMain = new UIPanel("Options", new Point(270, 100), new Size(260, 260));

            btnBack = new UIButton("Back", new Point(300, 300))
            {
                OnClick = delegate
                {
                    Game.Controller.Model.ActivateScene(GameSceneID.MainMenu);
                }
            };

            lblMusicVolume = new UILabel("Music volume", new Point(300, 140)) { ShowBackgroudPanel = false };

            imgMusicOff = new UIImage(Properties.Resources.ui, new Rectangle(new Point(398, 16), new Size(22, 20)), new Point(btnBack.Position.X + btnBack.Size.Width - 22, 160));
            imgMusicOn = new UIImage(Properties.Resources.ui, new Rectangle(new Point(398, 39), new Size(22, 20)), new Point(btnBack.Position.X, 160));

            barMusicLevel = new UIBar(new Point(imgMusicOn.Position.X + imgMusicOn.Size.Width + 2, 160), new Size(150, 20), 0f, 1f, GameSoundManager.Current.MusicVolume);

            AddChildren(new GameObject[] { imgBackground, pnlMain, lblMusicVolume, imgMusicOff, imgMusicOn, barMusicLevel, btnBack });
        }
    }
}
