﻿using System.Collections.Generic;
using System.Timers;

namespace MechBD
{
    public class GameTimerService
    {
        public static GameTimerService Instance = new GameTimerService();

        private List<Timer> _timers;

        private GameTimerService()
        {
            _timers = new List<Timer>();
        }

        public void RegisterTimer(Timer timer)
        {
            if (!_timers.Contains(timer)) _timers.Add(timer);
        }

        public void StopAll()
        {
            _timers.ForEach(t => t.Stop());
        }
    }
}
