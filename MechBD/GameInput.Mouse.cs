﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MechBD.Events;

namespace MechBD
{
    public class MouseInput
    {
        public List<MouseButtons> Buttons { get; private set; }
        public Point Position { get; private set; }

        public bool IsMouseInGameBounds { get; private set; }
        public bool IsLeftMouseDown => Buttons.Contains(MouseButtons.Left);

        public OnMouseMovedEventHandler OnMouseMoved;
        public OnMouseClickedEventHandler OnMouseClicked;

        public MouseInput(Form view)
        {
            Buttons = new List<MouseButtons>();
            Position = Point.Empty;

            view.MouseClick += OnMouseClick;

            view.MouseDown += OnMouseDown;
            view.MouseUp += OnMouseUp;

            view.MouseLeave += delegate { IsMouseInGameBounds = false; };
            view.MouseEnter += delegate { IsMouseInGameBounds = true; };

            view.MouseMove += OnMouseMove;
        }

        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            RegisterMousePosition(e);
            OnMouseClicked?.Invoke(Position, e.Button);
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            Buttons.Add(e.Button);
            RegisterMousePosition(e);
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            Buttons.Remove(e.Button);
            RegisterMousePosition(e);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            RegisterMousePosition(e);
            OnMouseMoved?.Invoke(Position);
        }

        private void RegisterMousePosition(MouseEventArgs e)
        {
            Position = new Point((int)(e.Location.X / GameCamera.Main.Scale.X), (int)(e.Location.Y / GameCamera.Main.Scale.Y));     
        }

        public bool IsMouseCursorInBoundsOf(Rectangle bounds)
        {
            return bounds.Contains(Position);
        }
    }
}
