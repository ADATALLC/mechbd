﻿namespace MechBD.Components
{
    interface IUpdateableComponent
    {
        void Update(double deltatime);
    }
}
