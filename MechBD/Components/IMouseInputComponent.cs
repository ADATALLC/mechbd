﻿using System.Drawing;
using System.Windows.Forms;

namespace MechBD.Components
{
    interface IMouseInputComponent
    {
        void OnMouseClicked(Point position, MouseButtons button);
        void OnMouseMoved(Point position);
    }
}
