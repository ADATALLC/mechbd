﻿namespace MechBD.Components
{
    public delegate void OnObservalblePropertyChangedEventHandler(object property);

    /// <summary>
    /// Generica observable proeprty object
    /// </summary>
    /// <typeparam name="T">Parameter type for observable to hold and change</typeparam>
    public class ObservableProperty<T>  where T : struct
    {
        private T _value;

        public OnObservalblePropertyChangedEventHandler OnObservalblePropertyChanged;

        public T Value
        {
            get => _value;
            set
            {
                _value = value;
                OnObservalblePropertyChanged?.Invoke(this);
            }
        }

        public ObservableProperty(T value)
        {
            _value = value;
        }
    }

    public class ObservableFloatProperty : ObservableProperty<float>
    {
        public ObservableFloatProperty(float value) : base(value) {}
    }
}
