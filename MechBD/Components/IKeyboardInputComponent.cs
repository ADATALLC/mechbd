﻿using System.Windows.Forms;

namespace MechBD.Components
{
    interface IKeyboardInputComponent
    {
        void OnKeyPressed(Keys button);
    }
}
