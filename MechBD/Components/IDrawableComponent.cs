﻿using System.Drawing;

namespace MechBD.Components
{
    interface IDrawableComponent
    {
        void Draw(Graphics gc);
    }
}
