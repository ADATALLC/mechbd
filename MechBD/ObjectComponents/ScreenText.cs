﻿using System;
using System.Drawing;
using MechBD.Components;

namespace MechBD.Objects
{
    public class ScreenText : GameObjectComponent, IDrawableComponent
    {
        public string Text { get; set; }
        public Font TextFont { get; private set; }
        public float TextFontHeight
        {
            get => _textFontSize;
            set
            {
                _textFontSize = value;
                FontFamily family = TextFont.FontFamily;
                TextFont?.Dispose();
                TextFont = new Font(family, _textFontSize);
            }
        }
        
        private float _textFontSize;

        public ScreenText(GameObject parent, string text, float size = 12f) : this(parent, text)
        {
            TextFontHeight = size;
        }

        public ScreenText(GameObject parent, string text) : base(parent)
        {
            Text = text;
            TextFont = new Font(GameFont.Instance.FontFamily ?? FontFamily.GenericMonospace, 12f);
            TextFontHeight = 12f;
        }

        public void Draw(Graphics gc)
        {
            SizeF measuredTextSize = gc.MeasureString(Text, TextFont);
            measuredTextSize.Width += 1f;

            RectangleF textLayout = new RectangleF(_parent.RenderableBounds.Location, measuredTextSize);

            textLayout.Y += (_parent.Size.Height - TextFontHeight) / 2f;
            textLayout.X += (_parent.Size.Width - textLayout.Width) / 2f;
            
            gc.DrawString(Text, TextFont, new SolidBrush(_parent.Color), textLayout);
        }
    }
}