﻿using System.Drawing;
using System.Timers;

using MechBD.Components;

namespace MechBD.Objects
{
    class AnimatedSprite : GameObjectComponent, IDrawableComponent
    {
        private Bitmap[] _frames;
        private Timer _timer;
        private double _timerThr = 1000 / 4;
        private int _frame;

        public AnimatedSprite(GameObject parent, Bitmap[] frames) : base(parent)
        {
            _frames = frames;
            _timer = new Timer(_timerThr);
            _timer.Elapsed += delegate {
                _frame = (_frames.Length != 0) ? (_frame + 1) % _frames.Length : 0;
            };
            _timer.Start();

            GameTimerService.Instance.RegisterTimer(_timer);
        }

        public void Draw(Graphics gc)
        {
            Rectangle dst = _parent?.RenderableBounds ?? Rectangle.Empty;

            gc.DrawImage((Bitmap)_frames[_frame].Clone(), dst);
        }
    }
}
