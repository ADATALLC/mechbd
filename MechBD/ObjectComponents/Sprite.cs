﻿using System.Drawing;
using System.Drawing.Imaging;
using MechBD.Components;

namespace MechBD.Objects
{
    public class Sprite : GameObjectComponent, IDrawableComponent
    {
        public Bitmap Image;

        public Sprite(GameObject parent, Bitmap image, Rectangle source) : this(parent, image)
        {
            Image?.Dispose();

            SaveImageData(image, source);
        }

        public Sprite(GameObject parent, Bitmap image) : base(parent)
        {
            SaveImageData(image, new Rectangle(Point.Empty, image.Size));
        }

        private void SaveImageData(Image image, Rectangle source)
        {
            Image = new Bitmap(image).Clone(source, PixelFormat.Format32bppArgb);
            Image.MakeTransparent(Color.White);            
        }

        public void Draw(Graphics gc)
        {
            Rectangle dst = _parent?.RenderableBounds ?? Rectangle.Empty;

            gc.DrawImage((Bitmap)Image.Clone(), dst);
        }
    }
}
