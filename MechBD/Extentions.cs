﻿using System.Drawing;

namespace MechBD
{
    static class Extentions
    {
        public static Rectangle Offset2(this Rectangle self, Point offset)
        {
            self.Offset(offset); return self;
        }

        public static Point Offset2(this Point self, int x, int y)
        {
            self.Offset(x, y); return self;
        }

        public static Point Offset2(this Point self, Point offset)
        {
            self.Offset(offset); return self;
        }
    }
}
