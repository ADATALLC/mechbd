﻿using System;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;

namespace MechBD
{
    class GameController
    {
        public GameModel Model;

        private GameView _view;
        private static bool _canUpdateView;
        
        /// <param name="view">GameView component</param>
        /// <param name="active">First GameScene to show on game start</param>
        /// <param name="scenes">List of all game scenes</param>
        public GameController(GameView view, GameScene active, GameScene[] scenes)
        {
            _view = view;
            _view.ClientSize = Properties.Settings.Default.Resolution;

            Model = new GameModel();
            Model.AddScene(active);
            Model.AddScenes(scenes);

            // set active scene
            Model.ActivateScene(active);

            // attach view controls
            GameInput.Mouse = new MouseInput(_view);
            GameInput.Mouse.OnMouseMoved += OnMouseMoved;
            GameInput.Mouse.OnMouseClicked += OnMouseClicked;
            GameInput.Keyboard = new KeyboardInput(_view);
            GameInput.Keyboard.OnKeyboardKeyPressed += OnKeyPressed;

            // attach view camera
            GameCamera.Main.AttachTo(_view);

            // start game timer
            GameTimer.Instance.OnUpdateEvent = UpdateFrame;
            GameTimer.Instance.Start();

            // allow view update 
            _canUpdateView = true;
        }

        private void OnKeyPressed(Keys button)
        {
            Model.OnKeyPressed(button);
        }

        private void OnMouseMoved(Point position)
        {
            Model.OnMouseMoved(position);
        }

        private void OnMouseClicked(Point position, MouseButtons button)
        {
            Model.OnMouseClicked(position, button);
        }

        private void UpdateFrame(double deltatime)
        {
            if (_canUpdateView)
            {
                _canUpdateView = false;

                GameCamera.Main.Update();

                Model.ActiveScene?.Update(deltatime);

                _view.DrawFrame(Model.ActiveScene);

                _canUpdateView = true;
            }
        }
    }
}
