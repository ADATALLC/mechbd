﻿using System.Collections.Generic;
using System.Windows.Forms;
using MechBD.Events;

namespace MechBD
{
    public class KeyboardInput
    {
        public List<Keys> Buttons { get; private set; }

        public OnKeyboardKeyPressedEventHandler OnKeyboardKeyPressed;

        public KeyboardInput(Form view)
        {
            Buttons = new List<Keys>();

            view.KeyUp += OnKeyUp;
            view.KeyDown += OnKeyDown;
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            Buttons.Remove(e.KeyData);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            Keys key = e.KeyData;

            if (!(e.Alt || e.Control || e.Shift || Buttons.Contains(key))) { Buttons.Add(key); }

            OnKeyboardKeyPressed?.Invoke(key);
        }
    }
}
