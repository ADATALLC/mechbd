﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using Newtonsoft.Json.Linq;

namespace MechBD.Objects
{
    public partial class TiledMap : GameObject
    {
        public Size MapSizeInTiles { get; private set; }
        public Size TileSize { get; private set; }

        public List<Bitmap> Tiles { get; private set; }
        public List<TileLayer> Layers { get; private set; }
        public List<TiledMapObject> Objects { get; private set; }

        public TiledMap(byte[] raw) : base()
        {
            RenderType = GameRenderType.Dynamic;

            Size = Properties.Settings.Default.Resolution;
            Position = Point.Empty;

            Tiles = new List<Bitmap>();
            Layers = new List<TileLayer>();
            Objects = new List<TiledMapObject>();
            
            JObject data = LoadMapDataFrom(raw);

            ReadTileSizeAndMapSize(data);

            LoadTileSets(data["tilesets"]);
            LoadLayers(data["layers"]);

            ResizeObjectsToFitMapData();
            
            AddChildren(Layers.ToArray());
        }
    }
}
