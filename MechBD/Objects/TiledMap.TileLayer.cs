﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Newtonsoft.Json.Linq;

namespace MechBD.Objects
{
    public partial class TileLayer : GameObject
    {
        private TiledMap _owner;
        
        public int[] Map { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public TileLayer(TiledMap owner, int width, int height, int[] map)
        {
            _owner = owner;
            
            Map = map;
            Width = width;
            Height = height;

            LoadTiles();
        }

        private void LoadTiles()
        {
            for (var y = 0; y < Height; y++)
            {
                for (var x = 0; x < Width; x++)
                {
                    int tileID = Map[x + y * Width];

                    if (tileID == 0) continue;

                    Bitmap tile = _owner.Tiles[tileID - 1];
                    Point position = new Point(x * tile.Width, y * tile.Height);

                    AddChild(new Tile(tile, position));
                }
            }
        }        
    }
}