﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Newtonsoft.Json.Linq;

namespace MechBD.Objects
{
    public partial class TiledMap
    {
        public static string LayerIsObjectGroup = "objectgroup";
        public static string LayerIsTileLayer = "tilelayer";

        private JObject LoadMapDataFrom(byte[] filedata)
        {
            if (filedata == null || filedata.Length <= 0)
            {
                throw new FileNotFoundException($"Can not find tilemaps data for level");
            }
            else
            {
                string a = System.Text.Encoding.UTF8.GetString(filedata);
                return JObject.Parse(a);
            }
        }

        private void ReadTileSizeAndMapSize(JToken data)
        {
            MapSizeInTiles = new Size(data["width"].Value<int>(), data["height"].Value<int>());
            TileSize = new Size(data["tilewidth"].Value<int>(), data["tileheight"].Value<int>());            
        }

        private void ResizeObjectsToFitMapData()
        {
            Size = new Size(MapSizeInTiles.Width * TileSize.Width, MapSizeInTiles.Height * TileSize.Height);
            Layers.ForEach(l => l.Size = Size);
        }

        private void LoadTileSets(JToken tilesets)
        {
            foreach (JObject tileset in tilesets.Children())
            {
                string source = $"{Properties.Settings.Default.AssetsPath}/{tileset["image"].Value<string>().Replace("../", "")}";
                string filepath = Path.GetFullPath(source);

                SplitTilesetToTiles(new Bitmap(filepath));
            }
        }

        private void LoadLayers(JToken layers)
        {
            foreach(JToken data in layers.Children<JToken>())
            {
                if (LayerIsObjectGroup.Equals(data["type"].Value<string>(), StringComparison.InvariantCultureIgnoreCase))
                {
                    ProccesAsObjectsGroup(data);
                } 
                else
                {
                    ProccesAsLayer(data);
                }
            }
        }

        private void SplitTilesetToTiles(Bitmap image)
        {
            int w = image.Width / TileSize.Width;
            int h = image.Height / TileSize.Height;
            Rectangle r = new Rectangle(Point.Empty, TileSize);

            for (int y = 0; y < h; y++)
            {
                r.Y = y * TileSize.Height;

                for (int x = 0; x < w; x++)
                {
                    r.X = x * TileSize.Width;
                    Tiles.Add(image.Clone(r, image.PixelFormat));
                }
            }
        }

        private void ProccesAsLayer(JToken data)
        {
            if (!data["visible"].Value<bool>()) return;

            int[] map = data["data"].ToObject<int[]>();
            int w = data["width"].Value<int>();
            int h = data["height"].Value<int>();

            Layers.Add(new TileLayer(this, w, h, map));
        }

        private void ProccesAsObjectsGroup(JToken data)
        {
            foreach (JToken obj in data["objects"].Children()) { ProcessObjectInObjectGroup(obj); }
        }

        private void ProcessObjectInObjectGroup(JToken data)
        {
            Objects.Add(new TiledMapObject(data["id"].Value<int>())
            {
                Name = data["name"].Value<string>(),
                Location = new Point(
                    data["x"].Value<int>(),
                    data["y"].Value<int>()),
                Size = new Size(
                    data["width"].Value<int>(),
                    data["height"].Value<int>())
            });
        }
    }
}
