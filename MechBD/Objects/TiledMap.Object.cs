﻿using System.Drawing;

namespace MechBD.Objects
{
    public struct TiledMapObject
    {
        public int ID { get; private set; }
        public string Name { get; set; }
        public Point Location { get; set; }
        public Size Size { get; set; }

        public TiledMapObject(int id)
        {
            ID = id;
            Name = "";
            Location = Point.Empty;
            Size = Size.Empty;
        }
    }
}
