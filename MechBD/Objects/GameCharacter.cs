﻿using System.Drawing;
using System.Windows.Forms;
using MechBD.Components;

namespace MechBD.Objects
{
    public partial class GameCharacter : GameObject, IKeyboardInputComponent
    {
        private Sprite _sprite;

        public double Speed { get; private set; }
        public int Lives { get; private set; }
        public bool IsStatic { get; set; }

        public GameCharacter(int lives = 1, double speed = 0, bool isStatic = false) : base()
        {
            Speed = speed;
            Lives = lives;
            IsStatic = isStatic;

            Size = new Size(48, 48);

            _sprite = new Sprite(this, Properties.Resources.Mech_Walk, new Rectangle(96, 0, Size.Width, Size.Height));

            AddComponent(_sprite);
        }
    }

    public partial class GameCharacter
    { 
        public void OnKeyPressed(Keys button)
        {
            if (IsStatic) return;
            
            if (button == Keys.Up) MoveUp();
            if (button == Keys.Down) MoveDown();
            if (button == Keys.Left) MoveLeft();
            if (button == Keys.Right) MoveRight();
        }

        private void MoveUp()
        {
            Position = Position.Offset2(0, (int)(-1 * GameTimer.Instance.DeltaTime * Speed));
        }

        private void MoveDown()
        {
            Position = Position.Offset2(0, (int)(1 * GameTimer.Instance.DeltaTime * Speed));
        }

        private void MoveRight()
        {
            Position = Position.Offset2((int)(1 * GameTimer.Instance.DeltaTime * Speed), 0);
        }

        private void MoveLeft()
        {
            Position = Position.Offset2((int)(-1 * GameTimer.Instance.DeltaTime * Speed), 0);
        }
    }
}
