﻿using System.Drawing;

namespace MechBD.Objects
{
    public partial class Tile : GameObject
    {
        private Sprite _sprite;
        
        public Tile(Bitmap image, Point position) : base()
        {
            RenderType = GameRenderType.Dynamic;

            Position = position;
            Size = image.Size;

            _sprite = new Sprite(this, image);

            AddComponent(_sprite);
        }
    }
}