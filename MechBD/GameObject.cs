﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MechBD.Components;

namespace MechBD
{
    public partial class GameObject : IUpdateableComponent, IDrawableComponent
    {
        public readonly Guid Id;

        public bool IsActive;

        public GameRenderType RenderType;

        public Color Color;
        public Point Position;
        public Size Size;

        public Rectangle Bounds => new Rectangle(Position, Size);
        public Rectangle RenderableBounds => (RenderType == GameRenderType.Static) ? Bounds : Bounds.Offset2(GameCamera.Main.Position);

        public GameObject()
        {
            Id = new Guid();
            IsActive = true;
            RenderType = GameRenderType.Dynamic;
            Children = new List<GameObject>();
            Components = new List<object>();
            Color = Color.Red;
            Position = Point.Empty;
            Size = Size.Empty;
        }

        public override string ToString() => Id.ToString();
    }


    public partial class GameObject
    {
        public List<object> Components { get; private set; }

        public GameObject AddComponent(GameObjectComponent component)
        {
            if (!Components.Contains(component)) Components.Add(component);

            return this;
        }
        public GameObject AddComponents(GameObjectComponent[] components)
        {
            Array.ForEach(components, c => AddComponent(c));

            return this;
        }

        public GameObject RemoveComponent(GameObjectComponent component)
        {
            Components.Remove(component);

            return this;
        }

        public List<T> GetComponents<T>()
        {
            return Components.FindAll(c => ((GameObjectComponent)c).IsActive && c is T)?.ConvertAll<T>(o => (T)o);
        }
    }

    public partial class GameObject
    {
        public List<GameObject> Children { get; private set; }

        public void AddChild(GameObject child)
        {
            if (!Children.Contains(child)) Children.Add(child);
        }

        public void AddChildren(GameObject[] children)
        {
            Array.ForEach(children, c => AddChild(c));
        }

        public void RemoveChild(GameObject child)
        {
            Children.Remove(child);
        }
    }

    public partial class GameObject
    {
        public virtual void Update(double deltatime)
        {
            if (!IsActive) return;

            GetComponents<IUpdateableComponent>()?.ForEach(c => c.Update(deltatime));

            Children.ForEach(c => c.Update(deltatime));
        }

        public virtual void Draw(Graphics gc)
        {
            if (!IsActive || !IsCameraShowsMe()) return;

            GetComponents<IDrawableComponent>()?.ForEach(c => c.Draw(gc));

            Children.ForEach(c => c.Draw(gc));
        }
    }


    public partial class GameObject
    {
        public bool IsMouseCursorInMyBounds()
        {
            return GameInput.Mouse?.IsMouseCursorInBoundsOf(RenderableBounds) ?? false;
        }

        public bool IsCameraShowsMe()
        {
            return GameCamera.Main.IsObjectShown(Bounds);
        }
    }         
}
